// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
  const sendmsg = await cloud.openapi.subscribeMessage.send({
    touser: event.touser,  // 要推送的用户openid
    page: "pages/index2/index2", // 要跳转的小程序界面
    data: {
      phrase7: event.phrase7,
      thing12: event.thing12,
    },
    templateId: 'JsMqLtQBsOFi6t0eKKwBqfSuUe9xSs9Yi__fYqSFRHE'
  });
  return sendmsg; // 返回执行结果
}
