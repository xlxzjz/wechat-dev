// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()


// 云函数入口函数
exports.main = async (event, context) => {
    let mode_str_list = ['water','toilet','washroom','laundry']
    let xid_str_list = ['zid','tid','wid','lid']
    let timeout_list = [600,780,900,600]
    for (let i = 0; i < mode_str_list.length; i++) {
      let mode_str = mode_str_list[i]
      let xid_str = xid_str_list[i]
      let timeout = timeout_list[i]
      let waitingList = null
      let lastUser = null
      await cloud.database().collection(mode_str)
        .where({
          token: 3,
        })
        .get().then(res => {
          waitingList = res
        });
      // 若超时 且队列里有下一位 则让给下一位 并通知他
      for (let j = 0; j < waitingList.data.length; j++) {
        let record = waitingList.data[j]
        await cloud.database().collection(mode_str)
          .where({
            token: 2,
            [xid_str]: record[xid_str]
          })
          .orderBy('time', 'desc')
          .limit(1)
          .get().then(res => {
            lastUser = res
          });
          if (lastUser.data.length > 0) {
            let lastUserTime = lastUser.data[0].time
            let diffTime = (new Date().getTime() - Date.parse(lastUserTime)) / 1000
            if (diffTime > timeout) {
              let updated = false
              await cloud.database().collection(mode_str)
                .where({
                  uid: record.uid,
                  token: 3
                })
                .update({
                  data: {
                    token: 2,
                    time: cloud.database().serverDate()
                  },
                }).then(res => {
                  updated = true
                });
                if(updated){
                  let mode_name;
                  if (mode_str == 'washroom') {
                    mode_name = '盥洗室'
                  }
                  if (mode_str == 'toilet') {
                    mode_name = '卫生间'
                  }
                  if (mode_str == 'water') {
                    mode_name = '打水'
                  }
                  if (mode_str == 'laundry') {
                    mode_name = '洗衣房'
                  }
                  cloud.openapi.subscribeMessage.send({
                    touser: record.uid,  // 要推送的用户openid
                    page: "pages/index2/index2", // 要跳转的小程序界面
                    data: {
                      phrase7: { value: '已到号' },
                      thing12: { value: mode_name },
                    },
                    templateId: 'JsMqLtQBsOFi6t0eKKwBqfSuUe9xSs9Yi__fYqSFRHE'
                  });
              }
            }
          }
        }
      
    }
}