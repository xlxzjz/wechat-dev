// app.js
App({
  globalData: {
    openID: null,
    building: '',
    floor: 5,
    nickName: '',
    avatarUrl: '',
    room: '',
    xid: {},
    timeout: {},
    temperatureZone: [],
    hasUserInfo: false,
    newUser: false,
    motto: '守得云开见月明'
  },

  onLaunch: function () {
    // 初始化云环境
    let that = this
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力');
    } else {
      wx.cloud.init({
        env: 'cloud1-0gqblqshc5bb5f9e',
        traceUser: true,
      });
    } // 获取用户openid
    wx.cloud.callFunction({
      name: 'quickstartFunctions',
      config: {
        env: 'cloud1-0gqblqshc5bb5f9e'
      },
      data: {
        type: 'getOpenId'
      }
    }).then((res) => {
      that.globalData.openID = res.result.openid
      // 检查数据库表里是否有此用户记录
      wx.cloud.database().collection('users').where({
        _openid: that.globalData.openID
      })
      .orderBy('time','desc')
      .limit(1)
      .get({
        success(result) {
          // console.log(result)
          if (result.data.length == 0) {
            // 如用户不存在，引导至注册页
            that.globalData.newUser = true
            that.globalData.hasUserInfo = true
          } else {
            // console.log(result)
            // 如用户已存在，则显示用户信息
            that.globalData.nickName = result.data[0].nickName,
            that.globalData.avatarUrl = result.data[0].avatarUrl,
            that.globalData.building = result.data[0].building,
            that.globalData.room = result.data[0].room,
            that.globalData.floor = result.data[0].floor,
            that.globalData.xid.zid=result.data[0].zid || null,
            that.globalData.xid.tid=result.data[0].tid || null,
            that.globalData.xid.wid=result.data[0].wid || null,
            that.globalData.xid.lid=result.data[0].lid || null,
            that.globalData.registerTime=result.data[0].time || null,
            that.globalData.version=result.data[0].version || null,
            that.globalData.hasUserInfo = true
          }
        }
      })
    }).catch((e) => {
    })
    // 获取全局配置
    wx.cloud.database().collection('config')
    .orderBy('time','desc')
    .limit(1)
    .get({
      success(result) {
        if(result.data.length>0){
          that.globalData.timeout=result.data[0].timeout || {}
          that.globalData.maxRank=result.data[0].maxRank || {}
          that.globalData.temperatureZone=result.data[0].temperatureZone || []
          that.globalData.motto=result.data[0].motto || '守得云开见月明'
          that.globalData.letter=result.data[0].letter || ''
          that.globalData.shareText=result.data[0].shareText || 'Bye'
          that.globalData.latestVersion = result.data[0].latestVersion || null
        }
      }
    })
  },
  watch: function(method){
    var obj = this.globalData;
    Object.defineProperty(obj,"hasUserInfo", {
      configurable: true,
      enumerable: true,
      set: function (value) {
        this._hasUserInfo = value;
        method(value);
      },
      get:function(){
       // 可以在这里打印一些东西，然后在其他界面调用getApp().globalData.name的时候，这里就会执行
        return this._hasUserInfo
      }
    })
  },
});
