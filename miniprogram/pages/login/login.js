var app = getApp()
Page({
  data: {
    disabled: true,
    avatarUrl: "",
    nickName: "",
    no: null,
    room: "",
    no_input: false,
    building_input: false,
    floor_input: false,
    room_input: false,
    building_index: null,
    building_picker: [],
    bid_list: [],
    floor_index: null,
    floor_picker: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33],
    modalShow: false,
  },
  onShow: function () {
    let that = this
    wx.cloud.database().collection('buildings')
      .orderBy('bid', 'asc')
      .get({
        success(result) {
          let bname_list = []
          for (let i = 0; i < result.data.length; i++) {
            let data = result.data[i]
            bname_list.push(data.bname)
            that.data.bid_list.push(data.bid)
          }
          that.setData({
            building_picker: bname_list
          })
        }
      })
  },
  // 选择器选择楼栋和楼层
  buildingPickerChange(e) {
    var that = this
    that.setData({
      building_index: e.detail.value,
      building_input: true
    })
    if (this.data.no_input == true && this.data.building_input == true && this.data.floor_input == true && this.data.room_input == true) {
      this.setData({ disabled: false });
    }
  },
  floorPickerChange(e) {
    // console.log(e);
    var that = this
    that.setData({
      floor_index: e.detail.value,
      floor_input: true
    })
    if (this.data.no_input == true && this.data.building_input == true && this.data.floor_input == true && this.data.room_input == true) {
      this.setData({ disabled: false });
    }
  },

  // 判断输入是否为空
  // noinput:function(e){
  //   this.setData({no:e.detail.value})
  //   this.setData({no_input:true})
  //   if(this.data.no_input==true && this.data.building_input==true && this.data.floor_input==true && this.data.room_input==true){
  //     this.setData({ disabled: false })
  //   }
  // },
  roominput: function (e) {
    this.setData({ room: e.detail.value })
    this.setData({ room_input: true })
    if (this.data.building_input == true && this.data.floor_input == true && this.data.room_input == true) {
      this.setData({ disabled: false })
    }
  },
  // 提交表单 
  formSubmit: function (e) {
    // 按钮灰掉
    this.setData({ disabled: true })
    var that = this
    // 用户确认信息
    wx.showModal({
      title: '请确认您提交的信息',
      content: '宿舍楼：' + that.data.building_picker[that.data.building_index] + '\r\n' + '楼层：' + that.data.floor_picker[that.data.floor_index] + '\r\n' + '房间号：' + that.data.room,
      cancelText: '取消',
      confirmText: '确认',
      success(response) {
        if (response.confirm) {
          console.log('用户点击确定')
          // 获取用户头像昵称
          wx.getUserProfile({
            desc: '用于完善用户资料',
            success: (res) => {
              // 将获取的用户信息赋值到data
              that.setData({
                avatarUrl: res.userInfo.avatarUrl,
                nickName: res.userInfo.nickName
              })
              // 写入数据表
              wx.showLoading()
              // 按照bid,room分配zid,tid
              var buildingName = that.data.building_picker[that.data.building_index]
              var floor = that.data.floor_picker[that.data.floor_index]
              var room = that.data.room
              var bid = that.data.bid_list[that.data.building_index]
              let tid = null
              let zid = null
              let wid = null
              let lid = null
              wx.cloud.database().collection('allocation')
                .where({ 'bid': bid, 'room': room })
                .get({
                  success(result) {
                    if (result.data.length > 0) {
                      tid = result.data[0].tid || null
                      zid = result.data[0].zid || null
                      wid = result.data[0].wid || null
                      lid = result.data[0].lid || null
                      wx.cloud.database().collection('users').add({
                        data: {
                          nickName: that.data.nickName,
                          avatarUrl: that.data.avatarUrl,
                          building: that.data.building_picker[that.data.building_index],
                          floor: that.data.floor_picker[that.data.floor_index],
                          zid: zid,
                          tid: tid,
                          wid: wid,
                          lid: lid,
                          room: that.data.room,
                          no: that.data.no,
                          time: wx.cloud.database().serverDate()
                        },
                        success(res) {
                          console.log('登录成功')
                          app.globalData.nickName = that.data.nickName,
                          app.globalData.avatarUrl = that.data.avatarUrl,
                          app.globalData.building = that.data.building_picker[that.data.building_index],
                          app.globalData.room = that.data.room,
                          app.globalData.floor = that.data.floor_picker[that.data.floor_index],
                          app.globalData.xid.zid = zid
                          app.globalData.xid.tid = tid
                          app.globalData.xid.wid = wid
                          app.globalData.xid.lid = lid
                          app.globalData.hasUserInfo = true
                          app.globalData.newUser = false
                          wx.hideLoading()
                          wx.showToast({
                            title: '成功登录',
                            icon: 'success',
                          })
                          that.goToIndex()
                        }
                      })
                    }
                    else{
                      wx.showToast({
                        title: '请检查宿舍号是否正确',
                        icon: 'error'
                      })
                    }
                  }
                })


            }
          })
        } else {
          that.setData({ disabled: false })
        }
      }
    })
  },

  // 导航到主页
  goToIndex() {
    wx.reLaunch({
      url: '/pages/index2/index2'
    })
  },

})

