// index.js
const app = getApp()
const util = require('../../utils/util')

Page({
  data: {
    //页面元素相关
    angle: 0,
    animation: '',
    animationStatus: true,
    interval: null,
    waterTop: 380,
    waveTop: 0,
    imgOpacity: 0,
    motto: '',
    letter: '',
    shareText: 'Bye'
  },


  onReady: function () {
    let that = this;
    wx.onAccelerometerChange(function (res) {
      var angle = -(res.x * 30).toFixed(1);
      if (angle > 14) {
        angle = 14;
      } else if (angle < -14) {
        angle = -14;
      }
      if (that.data.angle !== angle) {
        that.setData({
          angle: angle
        });
      }
    });
    that.setData({
      interval: setInterval(that.toggle, 1226),
      shareText: app.globalData.shareText
    })    
  },

  onFree(){
    let that = this
    clearInterval(that.data.interval)
    setInterval(that.overflow, 40)
  },
  overflow(){
    let that=this
    if(that.data.waterTop>-380){
      that.setData({
        waterTop: that.data.waterTop-6,
        waveTop: that.data.waveTop-7,
      })
    }
    if(that.data.waterTop<-150 && that.data.imgOpacity<1){
      that.setData({
        imgOpacity: that.data.imgOpacity+0.05
      })
    }
    if(that.data.waveTop==-154){
      that.typeMotto()
      that.typeLetter()
    }
  },
  typeMotto(){
    let str = app.globalData.motto
    let str_ = ''
    let i = 0
    let that = this
    let timer = setInterval(()=>{
        if(str_.length<str.length){
            str_ += str[i++]
            that.setData({motto: str_+'_'})
        }else{ 
            clearInterval(timer)
            that.setData({motto: str_})
        }
    },777)
  },
  typeLetter(){
    let str = app.globalData.letter
    str = str.replace('brbr','\n\n')
    let str_ = ''
    let i = 0
    let that = this
    let timer = setInterval(()=>{
        if(str_.length<str.length){
            str_ += str[i++]
            that.setData({letter: str_+'_'})
        }else{ 
            clearInterval(timer)
            that.setData({letter: str_})
        }
    },222)
  },
  toggle() {
    let that=this
    that.setData({
      animation: 'shake',
    })
    setTimeout(function () {
      that.setData({
        animation: ''
      })
    }, 888)
  },
  // 分享页面
  onShareAppMessage() {
    let that = this
    console.log(that.data.shareText)
    return {
      title: that.data.shareText,
      path: '/pages/index/index',
      imageUrl: '/images/more/61.png'
    }
  },
  onShareTimeline: function (res) {
    let that = this
    return {
      title: that.data.shareText,
      path: '/pages/index/index',
      imageUrl: 'https://tju-spring-memory-1259235122.cos.ap-shanghai.myqcloud.com/dundundun.jpg',
    }
  },
  goToIndex() {
    wx.reLaunch({
      url: '/pages/index2/index2'
    })
  },
});