#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 15:25:58 2022

@author: wzb
"""

import pandas as pd
import math

# data_toilet=pd.read_excel("0305.xlsx", "洗手间", dtype='object')
data_water=pd.read_excel("1102.xlsx","打水", dtype='object')
# data_washroom=pd.read_excel("0305.xlsx","盥洗室", dtype='object')

# bid编码 4位 第一位：1-嘉定 0-四平 ； 后三位：楼宇编号（西南：2；西北：1；学：3）（友园：1；朋园：0）例子：西南十一楼-0211；西北二-0102
# tid编码 7位 bid+楼层+设施编号
# zid编码 6位 bid+楼层号

data_dict={}

for index, row in data_water.iterrows():
    allocation = row['包含的寝室，寝室间请务必以中文顿号分隔']
    if type(allocation)!=type('String'): continue
    bid = row['bid']
    floor = str(row['楼层'])

    area = str(row['层内区域'])
    room_list = allocation.split('、')
    if bid not in data_dict.keys():
        data_dict[bid]={}
    for room in room_list:
        zid=bid+floor.zfill(2)+area
        if room not in data_dict[bid].keys():
            data_dict[bid][room]={}
        data_dict[bid][room]['zid']=zid
        
# for index, row in data_toilet.iterrows():
#     allocation = row['包含的寝室（请以中文顿号分割）']
#     if type(allocation)!=type('String'): continue
#     bid = row['bid']
#     floor = str(row['楼层'])
#     capmpus = row['校区']
#     area = str(row['层内区域/设施'])
#     room_list = allocation.split('、')
#     if bid not in data_dict.keys():
#         data_dict[bid]={}
#     for room in room_list:
#         tid=bid+floor.zfill(2)+area
#         if room not in data_dict[bid].keys():
#             data_dict[bid][room]={}
#         data_dict[bid][room]['tid']=tid

# for index, row in data_washroom.iterrows():
#     allocation = row['包含的寝室（请以中文顿号分割）']
#     if type(allocation)!=type('String'): continue
#     bid = row['bid']
#     floor = str(row['楼层'])
#     capmpus = row['校区']
#     area = str(row['层内区域/设施'])
#     room_list = allocation.split('、')
#     if bid not in data_dict.keys():
#         data_dict[bid]={}
#     for room in room_list:
#         wid=bid+floor.zfill(2)+area
#         if room not in data_dict[bid].keys():
#             data_dict[bid][room]={}
#         data_dict[bid][room]['wid']=wid

data_list=[]

for bid in data_dict.keys():
    for room in data_dict[bid].keys():
        data={}
        data['bid']=bid
        data['room']=room
        if 'lid' in data_dict[bid][room].keys():
            data['lid']=data_dict[bid][room]['lid']
        if 'wid' in data_dict[bid][room].keys():
            data['wid']=data_dict[bid][room]['wid']
        if 'tid' in data_dict[bid][room].keys():
            data['tid']=data_dict[bid][room]['tid']
        if 'zid' in data_dict[bid][room].keys():
            data['zid']=data_dict[bid][room]['zid']
        data_list.append(str(data))

with open('allocation.json','w') as f:
    f.write('\n'.join(data_list).replace("'",'"'))